# Script para creacion de base de datos MySql y tabla

    create database rest_bd;

    create table APP_USER (
       id BIGINT NOT NULL AUTO_INCREMENT,
       name VARCHAR(30) NOT NULL,
       age  INTEGER NOT NULL,
       salary REAL NOT NULL,
       PRIMARY KEY (id)
    );
       
    /* Populate USER Table */
    INSERT INTO APP_USER(name,age,salary)
    VALUES ('Sam',30,70000);
       
    INSERT INTO APP_USER(name,age,salary)
    VALUES ('Tom',40,50000);
     
    commit;
    
# Script para cargar datos dummy en la aplicación
    
    src/main/resources/dummy_data/carga_datos_dummy.sql
    
# Construir Aplicación

    mvn clean install

# Ejecutar
 
 ## Usar H2

    java -jar target/SpringBootCRUDApplicationExample-1.0.0.jar --spring.profiles.active=local    
    
## Usar MySql
    
    java -jar target/SpringBootCRUDApplicationExample-1.0.0.jar --spring.profiles.active=prod
    
    
# Ruta Navegador
    
    http://localhost:8080/SpringBootCRUDApp/
    