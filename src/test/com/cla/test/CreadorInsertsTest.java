package com.cla.test;

import org.junit.Test;

/**
 * Created by alaya on 27/11/17.
 */
public class CreadorInsertsTest {

    private static final  String MAYUS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final  String MINUS =    MAYUS.toLowerCase();
    private static final  String DIGITS =    "0123456789";
    private static final  String ALPHA_NUM =   MAYUS + MINUS;


    public String crearStringAleatorio(){

        StringBuilder sb = new StringBuilder();

        int cantidad = ALPHA_NUM.length() - 1 ;
        int tamano = 10;

        for (int i = 0; i < tamano ; i++){
            sb.append(ALPHA_NUM.charAt((int) (Math.random() * cantidad) + 1 ));
        }

       // System.out.println(sb.toString());

        return sb.toString();
    }

    @Test
    public void creaValues(){

        String base = "INSERT INTO APP_USER(name,age,salary) VALUES ";
        String baseValues = "('*', _, -), ";
        StringBuilder query = new StringBuilder();
        query.append(base);


        for (int i = 0; i < 10000; i++){

            baseValues = baseValues.replace("*", crearStringAleatorio());
            baseValues = baseValues.replace("_", String.valueOf((int) ((Math.random() * 100) + 1) ) );
            baseValues = baseValues.replace("-", String.valueOf((int) ((Math.random() * 1000000) + 1)) );

            query.append(baseValues);
            baseValues = "('*', _, -), ";
        }

        System.out.println(query.toString());

    }

    @Test
    public void stringReplaceTest(){
        String texto = "hola * como estas";


        System.out.println(texto.replace("*", "claudio"));
    }
}
