package com.cla.test;

/**
 * Created by alaya on 21/11/17.
 */
public class Persona {

    private String nombre;
    private Integer edad;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }


    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Persona persona = (Persona) o;

        if (!nombre.equals(persona.nombre)) return false;
        return edad.equals(persona.edad);
    }

    @Override
    public int hashCode() {
        int result = nombre.hashCode();
        result = 31 * result + edad.hashCode();
        return result;
    }
}
